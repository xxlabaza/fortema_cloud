/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.xxlabaza.test.fortema.dummy;

import com.netflix.appinfo.EurekaInstanceConfig;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author Artem Labazin <xxlabaza@gmail.com>
 * @since 21.11.2016
 */
@RestController
class MyController {

    @Autowired
    private EurekaInstanceConfig eurekaInstanceConfig;

    @RequestMapping("/{name}")
    public String greeting (@PathVariable("name") String name, HttpServletResponse response) {
        response.setHeader("X-INSTANCE-ID", eurekaInstanceConfig.getInstanceId());
        return "Hello " + name;
    }
}
